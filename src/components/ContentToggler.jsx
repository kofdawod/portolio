import { useState } from 'react';
import styles from './ContentToggler.module.css';
/**
 * creer une fonction pour cacher et afficher du contenue jsx dabs le site web 
 */
export default function ContentToggler(props) {
    const [visible, setVisible] = useState(false);
    /**fonction de visibiliter de toggle et son cache */
    const toggle = () => {
        setVisible(!visible);
    }

    return <div className={styles.box}>
        <div className={styles.title} onClick={toggle}>
            {/**importer l'image de togguel qui change d'etat dans l'activation  */}
            <svg className={visible ? styles.inverse : ''} xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 21l-12-18h24z"/></svg>
            {props.title}
        </div>

        {visible && 
            <div>{props.children}</div>
        }

    </div>
}