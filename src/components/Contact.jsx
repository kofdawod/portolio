import "./contact.css";
import Phone from "../img/phone.png";
import Email from "../img/email.png";
import Address from "../img/address.png";
import { useRef, useState } from "react";
import emailjs from "emailjs-com";

/**
 * creer  une constante Contact pour  la remplire et la exporter ver app.jsx
 */
const Contact = () => {
  const formRef = useRef();
  const [done, setDone] = useState(false)

/**
 * pour m'envoi un message a mon email si l'utilisateur a voulez me contacter
 *  cette  const hander submit on l'autilise dabs onSubmit pour m'evoi un mail 
 *  @param {handelSubmit} e 
 */

  const handleSubmit = (e) => {
    e.preventDefault();
    emailjs
      .sendForm(
        "service_nfjuvuq",
        "template_eof2pfs",
        formRef.current,
        "user_C9RMSJITxaoQedKVkPXcD"
      )
      .then(
        (result) => {
          console.log(result.text);
          setDone(true)
        },
        (error) => {
          console.log(error.text);
        }
      );
  };
/**
 * le contenu la page contact
 */
  return (
    <div className="c">
      <div className="c-wrapper">
        <div className="c-left">
          <h1 className="c-title">Let's discuss your project</h1>
          <div className="c-info">
            <div className="c-info-item">
              <img src={Phone} alt="" className="c-icon" />
              +1 438 459 8039
            </div>
            <div className="c-info-item">
              <img className="c-icon" src={Email} alt="" />
              kofdawod@gmail.com
            </div>
            <div className="c-info-item">
              <img className="c-icon" src={Address} alt="" />
              396 blake blvd, Ottawa Ontario, K1L 6L2
            </div>
            <iframe title="carte" className="c-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d21552.190825427002!2d-75.65480530828432!3d45.43375062799807!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4cce056865a1a509%3A0x1260b1a4c447851d!2s396%20Blake%20Blvd%2C%20Vanier%2C%20ON%20K1L%206L2!5e0!3m2!1sfr!2sca!4v1644191487650!5m2!1sfr!2sca" width="400" height="300"></iframe>

          </div>
        </div>
        <div className="c-right">
          <p className="c-desc">
            <b>What’s your story?</b> Get in touch. Always available for
            your project comes along. me.
          </p>
            {/* la form a remplire et envoier le message ecrit vers mon mail on utilisant onSubmit  */}
          <form ref={formRef} onSubmit={handleSubmit}>
            <input type="text" placeholder="Name" name="user_name" />
            <input type="text" placeholder="Subject" name="user_subject" />
            <input type="text" placeholder="Email" name="user_email" />
            <textarea  rows="5" placeholder="Message" name="message" />
            <button>Submit</button>
            {done && "Thank you..."}
          </form>  
        </div>       
      </div>
    </div>
  );
};

export default Contact;
