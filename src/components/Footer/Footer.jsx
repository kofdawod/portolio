import styles from './Footer.module.css'
/**
 * css dans le javascripte et l'utiliser dans les styles de jsx
 *//*
var style = {
    backgroundColor: "#F8F8F8",
    borderTop: "1px solid #E7E7E7",
    textAlign: "center",
    padding: "20px",
    position: "fixed",
    left: "0",
    bottom: "0",
    height: "60px",
    width: "100%",
}

var phantom = {
  display: 'block',
  padding: '20px',
  height: '60px',
  width: '100%',
}
*/
/**donction footer qui contien mon num et copyright */
function Footer({ children }) {
    return (
        <div>
            <div  className={styles.phantom} />
            <div className={styles.style}>
                { children }
               <p> <span>&copy;</span>  DERQUAOUI DAWOD </p>
            </div>
        </div>
    )
}
/**exporter la fonction */
export default Footer