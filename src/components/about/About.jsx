import "./about.css";
/**
 * importation des image
 */
import frog from "../../img/books/frog.jpg";
import formulede from "../../img/books/laformulededieu.jpg";
import pereRiche from "../../img/books/pereriche.jpg";

import pop from "../../img/games/popww.jpg";
import farcry from "../../img/games/farcry.jpg";
import rog from "../../img/games/rog.jpg";

import ContentToggler from "../ContentToggler";
import me2 from '../../img/me2.jpg';
/**
 * creation de la constante about pour la exporte ver app.jsx
 * @returns about
 */
const About = () => {
  return (
    <div className="a">
      {/* partie gauche de la page qui contien une image fixe  qui ne change pas de dim */} 
        <div className="a-left">
        <div className="a-card bg"></div>
        <div className="a-card">
          <img
            src={me2}
            alt=""
            className="a-img"
          />
        </div>
      </div>
      {/**partie droite de la page qui contien des information sur moi */}
      <div className="a-right">
        <h1 className="a-title">About Me</h1>
        <p className="a-sub">
         dawod derquaoui un programmeur marocain, ne a meknes en 1995, mon histoir avec l'ordinateur commence en 2000 j'ai ue mon premier pc avec 2 disquette une de prince of persia et une des jeux des echec, mais je prefer prince of persia biensur. apres ca ma carrier en jeux video commence j'ai bien aimer de jouer et participe a des tournois. apres avoir jouer les jeux de ubisoft prince of percia , far cry et assassin's creed j'ai vouler devenir un programeur de jeux et particier a creer des jeu comme assassins creed et prince of percia.
         les livre et les jeux videos sont mes sources  d'inspiration et la ou j'apprend plusieurs informations. 

        </p>
        <p className="a-desc">
    j'ai etudier les mathematiques appliquees a la fac de science moulay ismail a meknes j'ai commencer a etudier la programmation et les langage de programmation comme c#, java et python apres des cours et des certifica sur coursera j'ai dessider de venir au canada et etudier la programmation informatique pour commencer mon projet de programmation des jeux et de l'inteligence artificiel   </p>
          
          {/**parti toggler content pour afficher des livre et jeu sur la page avec des photo et description */}
    <ContentToggler title=" Books i like">  
        <div className="a-award">
          <img src={frog} alt="" className="a-award-img" />
          <div className="a-award-texts">
            <h4 className="a-award-title">Eat That Frog!</h4>
            <p className="a-award-desc">

            Il est temps d'arrêter de procrastiner et de faire plus de choses importantes ! Après tout, les gens qui réussissent n'essaient pas de tout faire. Ils se concentrent sur leurs tâches les plus importantes et les accomplissent. Ils mangent leurs grenouilles.            </p>
          </div>
        </div>
        <div className="a-award">
          <img src={formulede} alt="" className="a-award-img" />
          <div className="a-award-texts">
            <h4 className="a-award-title">La Formule de Dieu</h4>
            <p className="a-award-desc">
            La Formule de Dieu est un roman du journaliste et écrivain portugais José Rodrigues dos Santos. Initialement édité en 2006, il est paru en France le 14 juin 2012, chez HC Éditions,
            </p>
          </div>
        </div>
        <div className="a-award">
          <img src={pereRiche} alt="" className="a-award-img" />
          <div className="a-award-texts">
            <h4 className="a-award-title">Père riche, père pauvre</h4>
            <p className="a-award-desc">
            Père riche, père pauvre est un livre de Robert Kiyosaki et de Sharon Lechter paru en 1997. De style autobiographique, Robert Kiyosaki utilise un ensemble de paraboles et d'exemples tirés de son propre parcours afin de souligner l'importance de développer son intelligence financière.
            </p>
          </div>
        </div>
        </ContentToggler>
        
        <ContentToggler title="Games i like">  
        <div className="a-award">
          <img src={pop} alt="" className="a-award-img" />
          <div className="a-award-texts">
            <h4 className="a-award-title">Serie Prince of Persia</h4>
            <p className="a-award-desc">
            Prince of Persia est une série de jeux vidéo d'action/plates-formes commencée en 1989 avec Prince of Persia, développé par Jordan Mechner pour Brøderbund.
            </p>
          </div>
        </div>
        <div className="a-award">
          <img src={farcry} alt="" className="a-award-img" />
          <div className="a-award-texts">
            <h4 className="a-award-title">Serie farcry</h4>
            <p className="a-award-desc">
            Far Cry est une série de jeux vidéo de tir à la première personne, nommé d'après le premier jeu de la série, Far Cry (2004). Celui-ci, développé par Crytek en Allemagne et édité par Ubisoft,
            </p>
          </div>
        </div>
        <div className="a-award">
          <img src={rog} alt="" className="a-award-img" />
          <div className="a-award-texts">
            <h4 className="a-award-title">Assassin's creed</h4>
            <p className="a-award-desc">
            Assassin's Creed est une série de jeux vidéo historique d'action-aventure et d'infiltration en monde ouvert, développée et éditée par Ubisoft.
            </p>
          </div>
        </div>
        </ContentToggler>
      </div>
    </div>
  );
};

export default About;
