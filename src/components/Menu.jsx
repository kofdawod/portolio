import styles from './Menu.module.css';


/**
 * barre de navigation menu en la fait exporter ver ehader pour l'afficher dan tou les entete de chaque page
 * @param {Menu } props 
 * @returns 
 */
export default function Menu(props) {
    return <nav>
        <ul className={styles.ul}>
             <li>
                <a href="#" onClick={props.toPage('Intro')}>Intro</a>
            </li>
            <li>
                <a href="#" onClick={props.toPage('projet')}>Projet</a>
            </li>
            <li>
                <a href="#" onClick={props.toPage('Contact')}>Concact</a>
            </li>
            <li>
                <a href="#" onClick={props.toPage('about')}>about</a>
            </li>          

        </ul>
    </nav>
}