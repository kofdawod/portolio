import ContentToggler from '../ContentToggler';
import image1 from '../../img/projet gestion stagiaire/photo1.jpg';
import image2 from '../../img/projet gestion stagiaire/photo2.jpg';
import image3 from '../../img/projet gestion stagiaire/photo3.jpg';
import photo1 from '../../img/gestion des etudiant/Capture.PNG';
import photo2 from '../../img/gestion des etudiant/Capture2.PNG';
import photo3 from '../../img/gestion des etudiant/Capture3.PNG';
import capture1 from '../../img/oplaisir restaurant/Capture1.PNG';
import capture2 from '../../img/oplaisir restaurant/Capture2.PNG';
import capture3 from '../../img/oplaisir restaurant/Capture3.PNG';
import capture4 from '../../img/oplaisir restaurant/Capture4.PNG';
import capture5 from '../../img/oplaisir restaurant/Capture5.PNG';
import capture6 from '../../img/oplaisir restaurant/Capture6.PNG';

import './projet.css'

export default function Projet() {

    return <main className='projetS'>
        <section className='proj'>
            <h2 className='titreProjet'>Projet 1</h2>
            <p>
                logiciele de gestion de stagiaire on utilisons C# comme langage de programmation creer par "Microsoft visual studio" ce programme est une interface graphique qui gere les stagiaire leur programe et trier et afficher des liste de stagiaire par chaque programe     </p>

            <ContentToggler title="des images sur le projet">
                <div className='stagiaire'>
            <div className='stagiaireImg'><img src={image1} alt="image1" /></div>
            <div className='stagiaireImg'><img src={image2} alt="image2" /></div>
            <div className='stagiaireImg'><img src={image3} alt="image3" /></div>
            </div>
            </ContentToggler>
        </section>
        <section className='proj'>
            <h2 className='titreProjet'>Projet 2</h2>
            <p>
            logiciel de gestion des etudiants et de note qui gere les etudiants inscrit et creer des module avec coeficien et donner une note pour avoir un bultin a la avec une note finale et grade             </p>
            <ContentToggler title="des images sur le projet">
                <div className='stagiaire'>
            <div className='stagiaireImg'><img src={photo1} alt="image1" /></div>
            <div className='stagiaireImg'><img src={photo2} alt="image2" /></div>
            <div className='stagiaireImg'><img src={photo3} alt="image3" /></div>
            </div>
            </ContentToggler>
            <div> </div>
        </section>

        <section className='proj'>
            <h2 className='titreProjet'>Projet 3</h2>

            <p>
               Un site web pour gestion des commande d'un restaurant avec un bon designe et des mode de payement different des formulaire a remplis pour les donnes personnel panier facile a utiliser et menu bien visible et en presentant les articles avec le cote administrateur pour gerer les commande et leurs etats 
             </p>

            <ContentToggler title="Titre à afficher">
            <div className='stagiaire'>
            <div className='stagiaireImg'><img src={capture1} alt="image1" /></div>
            <div className='stagiaireImg'><img src={capture2} alt="image2" /></div>
            <div className='stagiaireImg'><img src={capture3} alt="image3" /></div>
            <div className='stagiaireImg'><img src={capture4} alt="image1" /></div>
            <div className='stagiaireImg'><img src={capture5} alt="image2" /></div>
            <div className='stagiaireImg'><img src={capture6} alt="image3" /></div>
            </div>
            </ContentToggler>
            <div> </div>
        </section>
    </main>
}