import "./intro.css";
import Me from "../img/me.jpg";

const Intro = () => {
  return (
    <div className="i">
      <div className="i-left">
        <div className="i-left-wrapper">
          <h2 className="i-intro">Hello, My name is</h2>
          <h1 className="i-name">DAWOD DERQUAOUI</h1>
          <div className="i-title">
            <div className="i-title-wrapper">
              <div className="i-title-item">Web Developer</div>
              <div className="i-title-item">Programmer</div>
              <div className="i-title-item">Pro Seller</div>
              <div className="i-title-item">Chess Player</div>
              <div className="i-title-item">Biker</div>           
            </div>
          </div>
          <p className="i-desc">
            programmeur des applications bureau et mobile.Web devloppement on utilisant React, HTML 5, CSS, JAVASCRIPT.<br></br>
            je fait la creation des bases de donnees, MySQL, Mongo_db, et programmer en C#, Java et Python. 
          </p>
        </div>
      </div>
      <div className="i-right">
        <div className="i-bg"><img src={Me} alt="" className="i-img" /></div>

      </div>
    </div>
  );
};

export default Intro;
