import Menu from "../Menu";
import styles from './Header.module.css';
import dawodlogo from '../../img/dawodlogo.png'

/**creer une fonction header et l'exporter pour l'utiliser dans app.jsx */
/**Afficher un Logo dans le header*/
/**appelle a le component Menu et l'afficher dans header */
export default function Header(props) {
    /**returner le header pour afficher ses elements */
    return <header className={styles.header}>

        <div className="MyLogo"><img src={dawodlogo} alt="Logo" className="logos"/></div>


        
        <div className="itemMenu"> <Menu toPage={props.toPage} />  </div>
        
        
        
    </header>
}
