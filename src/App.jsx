import { useState } from 'react';
import Header from './components/Header/Header';
import Projet from './components/projet/Projet';
import About from './components/about/About';
import Contact from './components/Contact';
import Intro from './components/Intro';
import Footer from './components/Footer/Footer';
import './App.css';

/**fonction de app qui contien tou le contenu de notre application de site web on a la fait exporter pour la utiliser dans index.jsx */
export default function App() {
  const [page, setPage] = useState('Intro');

  const toPage = (page) => {
    return () => {
      setPage(page);
    }
  }

  return <>
    <Header toPage={toPage} />
    
    {page === 'projet' &&
      <Projet />
    }
    
    {page === 'Contact' &&
      <Contact />
    }

    {page === 'about' &&
      <About />
    }

    {page === 'Intro' &&
      <Intro />
    }
   
    <Footer/>
  </>;
}
